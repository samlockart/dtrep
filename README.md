# dtrep - a tool for managing dragontail repositories

dtrep is a bash script which wraps around *aptly*, which is a repository management tool.

dtrep allows for the addition and publication of custom debian packages to be served over HTTPS.
