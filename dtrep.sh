#!/usr/bin/env bash
################################################################################
# dtrep.sh - a script to allow for easy management of our repositories
# author: Sam Lockart
# depends: awscli, aptly
# usage: please run this script with the 'help' paramter to display usage info
# error codes:
E_NO_ROOT=1 # We are not root
E_STAGE_DIR=2 # Unable to write to the staging directory
E_NO_APTLY=3 # Aptly is not installed correctly
E_NO_CONFIG=4 # The configuration file was unable to load
E_NO_BACKUP=5 # The snapshot / backup failed to create
E_S3_SYNC=6 # Failed to sync packages from specified S3 bucket
E_NO_FILES=7 # No debian files to add to aptly
E_NO_IMPORT=8 # Failed to import the files into aptly
E_REPO_PUB=9 # Failed to publish the repository
E_REPO_CR=10 # Failed to create the repo
E_GPG_SIGN=11 # Failed to sign the InRelease + Release files with SHA256
################################################################################

# declares some variables for internal use (do NOT change)
set -euo pipefail
IFS=$'\n\t'
cd "$(dirname "$0")"
CONFIG='./dtrep.conf'

################################################################################
# Function: load_config
# Parameters: N/A.
# Functionlity: loads the configuration file and fails with E_NO_CONFIG if it
#               doesn't exist
# Return codes: $E_NO_CONFIG
################################################################################
load_config() {
    if [[ -f $CONFIG ]]; then
        source ${CONFIG}
        S3_BUCKET=${S3_BUCKET:5} # strips off URI
        GPG_PASSWORD=''
    else
        err "FATAL: $CONFIG not found"
        return ${E_NO_CONFIG}
    fi
}


################################################################################
# Function: err 
# Parameters:
#    - Error message text
# Functionlity: log a message to the STDERR.
# Return codes: N/A.
################################################################################
err() {
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}

################################################################################
# Function: log
# Parameters:
#    - Log message text
# Functionlity: log a message to the STDOUT.
# Return codes: N/A.
################################################################################
log() {
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&1
}

################################################################################
# Function: precheck 
# Parameters: N/A.
# Functionlity: performs a range of checks to ensure the script will be able to run 
# Return codes: $E_NO_ROOT, $E_STAGE_DIR, $E_NO_APTLY
################################################################################
precheck() {
    log 'Started prechecks'

    if (( $EUID == 0 )); then
        err 'FATAL: This script should not be run as root. Exiting...'
        return ${E_NO_ROOT}
    fi

    mkdir -p "$STAGING_FOLDER" &>/dev/null || true
    if [[ ! -w "$STAGING_FOLDER" ]]; then
        err "FATAL: $STAGING_FOLDER is not writable. Please provide a writeable working directory."
        return ${E_STAGE_DIR}
    fi

    if [[ -z $(command -v aptly 2>/dev/null) ]]; then
        err "FATAL: aptly is not installed correctly"
        return ${E_NO_APTLY}
    fi

    if ! aws s3 ls &>/dev/null; then
        err 'WARNING: awscli needs to be configured before using the sync command. Please do this with "aws configure".'
    fi

    log "Prechecks finished on $HOSTNAME"
}

################################################################################
# Function: clean_staging
# Functionlity: displays usage information
# Return codes: N/A.
################################################################################
clean_staging() {
    log 'Cleaning up staging directory and exiting'
    if rm -r "${STAGING_FOLDER:?}/" &>/dev/null; then
        log 'Staging directory cleared'
    else
        err 'WARNING: Could not clear staging directory, please check that the staging directory is writable'
    fi
}

################################################################################
# Function: create_table / fill_table 
# Parameters: N/A.
# Functionlity: Build and draw a table based on the packages available in the
#               repository
# Return codes: N/A.
################################################################################
create_table() {
    fill_table() {
        shopt -s globstar
        PACKAGE_PATH=($HOME/public/**/*.deb)
        for package in "${PACKAGE_PATH[@]}"; do
            NAME=$(dpkg-deb -W "$package" | awk '{print $1}')
            MD5SUM=$(md5sum "$package" | awk '{print $1}')
            VERSION="$(dpkg-deb -W "$package" | awk '{print $2}')"
            printf "%-30s %-30s %-30s\n" "$NAME" "$VERSION" "$MD5SUM"
        done
    }
    fill_table | sed -e '1s/^/Package Version MD5sum\n======== ======== ========\n/' | awk 'NR<3{print $0; next}{print $0 | "sort" }' | column -t
}


################################################################################
# Function: status 
# Parameters: N/A.
# Functionlity: displays information about the aptly repository and snapshots
# Return codes: N/A.
################################################################################
status() {
    printf "Status of $REPO:\n"
    create_table 
    printf '\n--------\nAvailable snapshots (Last 5):\n\n'
    aptly snapshot list | tail -n 5
}

################################################################################
# Function: sign_release 
# Parameters: N/A.
# Functionlity: Aptly by default signs the release file with SHA1. Current
#               version of Ubuntu + Debian require at least SHA256 signing.
# Return codes: $E_GPG_SIGN
################################################################################
sign_release() {
    cd "$HOME/public/dists/${DISTRIBUTION}/"
    if gpg --digest-algo SHA256 --yes --passphrase ${GPG_PASSWORD} --clearsign -o InRelease Release &&\
            gpg --digest-algo SHA256 -abs --yes --passphrase ${GPG_PASSWORD} -o Release.gpg Release; then
        log "Successfully signed repository with GPG/SHA256!"
        cd "$OLDPWD"
        return 0
    else
        err "[FATAL] Unable to sign $REPO with GPG/SHA256: gpg exited with $?"
        return ${E_GPG_SIGN}
    fi
}


################################################################################
# Function: backup_current_repo
# Parameters: N/A.
# Functionlity: creates a snapshot of the current repository and gives it a name
#               of the date + a random string
# Return codes: $E_NO_BACKUP
################################################################################
backup_current_repo() {
    log "Creating a snapshot of $REPO"
    _SNAP_NAME="$(date +'%Y-%m-%d-'"$(cat /proc/sys/kernel/random/uuid)")"
    if aptly snapshot create "$_SNAP_NAME" from repo "$REPO"; then
        log "Created snapshot: $_SNAP_NAME"
    else
        err "WARNING: Failed to create snapshot: aptly exited with $?"
        return ${E_NO_BACKUP}
    fi
}

################################################################################
# Function: usage
# Parameters: N/A.
# Functionlity: displays usage information
# Return codes: N/A.
################################################################################
usage() {
    echo ''
    echo "·▄▄▄▄  ▄▄▄   ▄▄▄·  ▄▄ •        ▐ ▄ ▄▄▄  ▄▄▄ . ▄▄▄·      ";
    echo "██▪ ██ ▀▄ █·▐█ ▀█ ▐█ ▀ ▪▪     •█▌▐█▀▄ █·▀▄.▀·▐█ ▄█▪     ";
    echo "▐█· ▐█▌▐▀▀▄ ▄█▀▀█ ▄█ ▀█▄ ▄█▀▄ ▐█▐▐▌▐▀▀▄ ▐▀▀▪▄ ██▀· ▄█▀▄ ";
    echo "██. ██ ▐█•█▌▐█ ▪▐▌▐█▄▪▐█▐█▌.▐▌██▐█▌▐█•█▌▐█▄▄▌▐█▪·•▐█▌.▐▌";
    echo "▀▀▀▀▀• .▀  ▀ ▀  ▀ ·▀▀▀▀  ▀█▄▀▪▀▀ █▪.▀  ▀ ▀▀▀ .▀    ▀█▄▀▪";
    echo ''
    echo "\
Usage: $0 [subcommand...]
(running $0 with no parameters will print this usage information)

The following subcommands are available for use:

         sync - synchronise packages from specified S3 bucket to staging folder
         add - imports packages from staging folder into the repository
         backup - creates a snapshot of the repository and labels it with current date + random string
         list - lists packages in the remote S3 bucket
         create - creates a new, empty repoistory as defined in the configuration file
         status - lists details about the repository and available backups
         check - runs some basic checks to determine if DragonRepo has been setup correctly
         help - displays this usage information
"

    return 0
}

################################################################################
# Function: create_repo 
# Parameters: N/A.
# Functionlity: Uses aptly to create a repository using the values in the
#               configuration file.
# Return codes: $E_CREATE_REPO 
################################################################################
create_repo() {
    log "Creating aptly repository ${REPO}"
    if aptly -distribution=${DISTRIBUTION} -comment=${COMMENT} -component=${COMPONENT} repo create ${REPO} &>/dev/null; then
        log "Created repository. Add packages to it using the 'add' subcommand"
    else
        err "$REPO could not be created as it most likely already exists"
        return ${E_REPO_CR}
    fi
}

################################################################################
# Function: list_s3_packages 
# Parameters: N/A.
# Functionlity: displays usage information
# Return codes: N/A.
################################################################################
list_s3_packages() {
    log "Listing packages in ${S3_BUCKET}"
    echo "Packages available in ${S3_BUCKET}:"
    aws s3 ls "s3://${S3_BUCKET}" --recursive | awk '/.deb$/ {print $4}'
    log 'Packages listed'
}

################################################################################
# Function: sync_s3_packages 
# Parameters: N/A.
# Functionlity: uses awscli to sync deb files from specified s3 bucket to the
#               staging folder
# Return codes: $E_S3_SYNC 
################################################################################
sync_s3_packages() {
    log "Syncing packages from ${S3_BUCKET} to ${STAGING_FOLDER}"
    if aws s3 sync "s3://${S3_BUCKET}" "${STAGING_FOLDER}" --exclude "*" --include "*.deb" &>/dev/null; then
        log 'Sync completed'
    else
        err "WARNING: Sync from ${S3_BUCKET} failed: aws exited with $?"
        return ${E_S3_SYNC}
    fi
}

################################################################################
# Function: add_packages
# Parameters: N/A.
# Functionlity: This function performs a count of the *.deb files in the staging
#               directory before trying to add them to the repo.
#               If there are no .deb files the script will print an error and exit
# Return codes: $E_NO_FILES, $E_NO_IMPORT
################################################################################
add_packages() {
    if create_repo; then
        add_packages # call ourselves again if create_repo returns 0
    else
        _COUNT=$(ls -1R ${STAGING_FOLDER} &>/dev/null | awk '/.deb/' | wc -l)
        if (( _COUNT == 0 )); then
            err 'WARNING: There are no *.deb files in the staging directory. Will sync for you.'
            if sync_s3_packages; then
                add_packages
            else
                err 'FATAL: There are no packages to add and we cannot sync from S3. You can manually place *.deb files in the staging folder'
                return ${E_NO_FILES}
            fi
        else
            log "Moving $_COUNT *.deb files from ${STAGING_FOLDER} to ${REPO}"
            if aptly repo add "$REPO" "$STAGING_FOLDER"; then
                log 'Packages successfully imported'
            else
                err "FATAL: Unable to import the debian packages: aptly exited with $?"
                return ${E_NO_IMPORT}
            fi
        fi
    fi
}

################################################################################
# Function: publish_repo 
# Parameters: N/A.
# Functionlity: this function publishes the imported packages
# Return codes: $E_REPO_PUB
################################################################################
publish_repo() {
    if [[ -z $GPG_PASSWORD ]]; then
        read -s -p "Unlock GPG signing key: " GPG_PASSWORD
        if : | gpg --passphrase "$GPG_PASSWORD" --no-use-agent -o /dev/null --local-user "$GPG_KEYID" -as -; then
            echo 'GPG key unlocked!'
        else
            err 'WARNING: Incorrect GPG key passphrase, try again...'
            GPG_PASSWORD=''
            publish_repo
        fi
    fi

    log "Publishing ${REPO}"

    if aptly -passphrase="$GPG_PASSWORD" -distribution=${DISTRIBUTION} publish repo ${REPO} &>/dev/null; then
        return 0
    else
        if aptly -force-overwrite -passphrase="$GPG_PASSWORD" publish update ${DISTRIBUTION} &>/dev/null; then # TODO: Currently GPG will STILL write to Stdout, fix soon
            sign_release # signs the repository with SHA256
            log "${REPO} published"
            return 0
        else
            return ${E_REPO_PUB}
        fi
    fi
}

main() {
    case "${1:-help}" in
        "")
            ;;
        sync)
            sync_s3_packages
            ;;
        add)
            add_packages || true
            publish_repo
            clean_staging
            ;;
        backup)
            backup_current_repo
            ;;
        list)
            list_s3_packages
            ;;
        create)
            create_repo
            ;;
        status)
            status
            ;;
        help)
            usage "$@"
            ;;
        check)
            precheck
            ;;
        *)
           usage "$@"
    esac
}

load_config
main "$@" > >(tee -a ${LOG_FILE}) 2> >(tee -a ${ERR_FILE} >&2)
